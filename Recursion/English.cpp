/**********************************************
* File: English.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* The main driver for the Draw Ruler Problem 
**********************************************/
#include "Supp.h"
#include "English.h"

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* The main driver function 
********************************************/
int main(int argc, char** argv){
	
	drawRuler(getArgv1Num(argc, argv), 8);
	
}
